----------------
Overview
----------------

Drush Scenario Builder (DSB) is a utility to support continuous integration by
automating installation, configuration and testing processes. It can be used
either for quick setup of development environments to allow automation of
repetitive tasks or to create a setup suitable for automated testing tools such
as jenkins.

----------------
Installation
----------------

Place the dsb.drush.inc file into any of the Drush command directories. I.e.
the ~/.drush directory. To verify that Drush picked up the utility type
drush help dsb.

DNS resolution
With each dsb scenario, corresponding hostnames need to be resolved. Scenarios
typically use the domain *.dev to point to the local machine. We recommend
you to use a local DNS server (such as pdns) and setup the wildcard properly
*.dev => 127.0.0.1
If missing, you need to add each of the hosts in /etc/hosts manually.

----------------
Creating scenarios
----------------

1) Create your workspace directory. This directory can be part of your project
   files, however during build process it must always reside outside the build
   target. In other words, you can bundle DSB files with your module, however
   prior to building scenarios you need to copy them into external workspace
   dir. Optionally, your build target directory may be a sub dir of the
   workspace directory.

2) Copy the example.config.php into your workspace and rename it to config.php.

3) Copy your drush make file into the workspace dir.

4) Create your local.config.php file in which you can override values in the
   config.php. This file should not be part of the project files in repository,
   but each one working on the project should have own copy with specific
   settings.

5) Adjust settings in the config.php according to your needs. For a quick start
   you can set the build_target to your current project directory. If doing so
   the build process will be skipped to the installation and configuration of
   individual scenarios. Mind that passing the option --force-rebuild will
   remove the build_target directory.

----------------
Anatomy
----------------

-- Health checks of system configuration (Optional with --skip-checks)
  |
-- Build project files as Drupal multisite
  |
  -- Remove project files (Optional via --force-rebuild option)
  |
  -- Run custom drush make script if provided, otherwise run default
  |  drush make makefile build_target
  |
  -- Run custom build script if provided
  |
  -- Create site directories and configure sites.php
  |
-- Scenarios
  |
  -- Run pre installation script if provided
  |
  -- Sites installation and configuration
    |
    -- Run install script if provided, otherwise run default drush site-install
    |
    -- Run setup script if provided
  |
  -- Run post installation script if provided
  |
-- Test suites (Optional via --run-tests option)
  |
  -- Run test script if provided, otherwise run default drush run-test for each
  |  test object
  |
-- Cleanup scenarios (Optional via --cleanup option)
  |
  -- Run cleanup script if provided
  |
  -- Clean sites
    |
    -- Drop databases

----------------
EXAMPLES
----------------

$drush dsb
  Builds all scenarios defined in the config.php file.

$drush dsb my_scenario
  Builds only my_scenario defined in the config.php file.

$drush dsb --run-tests
  Builds all scenarios and runs tests.

$drush dsb --run-tests --cleanup
  Builds all scenarios, runs tests and cleanup.

$drush dsb --force-rebuild
  Prior to building scenarios it will delete all sources in the build_target if found.

----------------
TODO
----------------

- Provide example scripts

- Pull scenarios from a remote repository.

- Add possible file structures into anatomy.

----------------
Troubleshooting
----------------

- If getting unresolved hosts errors make sure you have correctly configured
  your DNS or added all used uris into your /etc/hosts file.

= In case of "Site already installed" error, remove sites/SITEs directories and
  start over.
