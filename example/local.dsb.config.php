<?php
/**
 * @file
 * Local DSB configuration.
 *
 * This file should not be part of the repository files but specific to each
 * environment where it gets setup.
 */

$config['build target'] = '/var/www/dsb_example/www';
$config['db url'] = 'mysql://root:q@localhost';
$config['domain'] = 'example.dev';
