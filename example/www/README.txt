
Build target directory. This is the place where project sources gets build. Note
that this does not have to reside inside of your workspace directory, but you
can control its location via $config['build target'] setting.
