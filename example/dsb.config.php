<?php
/**
 * @file
 * DSB config file for the example workspace.
 */

$config['build tag'] = 'dsb_example';

// Set by local.dsb.config.php
$config['build target'] = '';
$config['db url'] = '';
$config['domain'] = 'example.dev';

$config['scenarios']['simple'] = array(
  'hosts' => array(
    'example' => array(
      'install profile' => 'standard',
    ),
  ),
);

$config['scenarios']['test_content'] = array(
  'hosts' => array(
    'example' => array(
      'install profile' => 'standard',
      'setup script' => array(
        'drush en devel_generate -y',
        'drush genc 10 3',
        'drush cc all'
      ),
    ),
  ),
);
$config['scenarios']['automated_tests'] = array(

  'cleanup script' => array(
    "chmod -R 777 \$BUILD_TARGET",
    "rm -R \$BUILD_TARGET"
  ),

  'hosts' => array(
    'example' => array(
      'install profile' => 'standard',
      'tests' => array(
        'test objects' => array('Blog'),
      ),
    ),
  ),
);

// Include local config file with config overrides.
if (file_exists(dirname(__FILE__) . '/local.dsb.config.php')) {
  include dirname(__FILE__) . '/local.dsb.config.php';
}
