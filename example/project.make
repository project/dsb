; Core version
; ------------

core = 7.x

; API version
; ------------
; Every makefile needs to declare its Drush Make API version. This version of
; drush make uses API version `2`.

api = 2

; Core project
; ------------

projects[drupal][version] = 7.17

; Modules
; --------

projects[devel][version] = 1.3
projects[devel][type] = "module"
