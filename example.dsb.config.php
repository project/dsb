<?php
/**
 * @file
 * Example DSB dsb.config.php file.
 *
 * dsb.config.php file is a common place to describe:
 *
 * - Project files build using drush make.
 *
 * - Scenarios builds, where each scenario can have multiple sites installed
 *   and configured.
 *
 * - Testing procedures triggered after sites installation and setup.
 *
 * - Cleanup tasks that are triggered after tests run.
 *
 * Note that shell scripts defined by the configuration can be either inline
 * commands or an external executable shell files. Moreover you can provide an
 * array of inline scripts or files or combination of both.
 *
 * Script files MUST BE LOCATED IN THE WORKSPACE and therefore you must provide
 * a path to the file within the workspace directory.
 *
 * For the shell scripts there are available shell variables that can be used
 * by your scripts. Even though this is not necessary and you can hard code
 * these values in scripts, it is not recommended as then it will not be
 * possible to do overrides in your local.dsb.config.php
 *
 * Never reset the $config variable as it will override the $config['workspace']
 * which is set before loading the dsb.config.php file.
 */

// Represents a namespace for current build.
// Required
$config['build tag'] = 'test_project';

// The workspace directory where dsb.config.php and build related files should be
// placed.
//
// Note that this setting is set before the dsb.config.php file and is available for
// the configuration options.
//
// Do not set its value here, but instead use the --workspace cmd option when
// invoking drush dsb command.
//
// Default value is the path of the directory where the drush dsb command was
// invoked.
//
// $config['workspace']

// Directory where the application will be built.
//
// Optional [$workspace/www]
$config['build target'] = '/var/www/project_dir/www';

// Drush make file.
//
// Optional [project.make]
$config['makefile'] = 'test_project.make';

// Custom drush make script to build project sources.
//
// Optional
$config['make script'] = 'drush make $MAKE_FILE --no-cache';

// Script that gets called on cleanup before databases are dropped.
//
// Default behaviour will just drop databases created during setup. In case
// you need to do additional cleanup, this is the place for it. Note that it
// is not necessary to delete built files as that can be done by next built
// by passing --force-rebuild=1 option into cmd.
//
// Optional
$config['cleanup script'] = 'rm -R ${BUILD_TARGET}';

// DB url without database name - it is automatically generated as
// [BUILD_TAG]_[HOST] for each site.
//
// Required
$config['db url'] = 'mysql://username:password@localhost';

// The base domain that will be used to determine actual domain of each site.
//
// Required
$config['domain'] = 'example.com';

// Scenarios configuration.
$config['scenarios']['example_scenario'] = array(

  // Script that gets called after drush make.
  //
  // This script can be used to do additional setup of project files. I.e.
  // downloading libraries, doing additional files manipulation, etc.
  //
  // Script variables:
  // - BUILD_TAG
  // - WORKSPACE
  // - BUILD_TARGET
  // - MAKE_FILE
  // - SCENARIO
  // - custom variables defined in the dsb.config.php
  //
  // Optional
  'build script' => 'dl_colorbox_lib.sh',

  // Variable name => Value pairs that will be passed into:
  // - build script
  // - install script
  // - setup script
  // - test script
  //
  // In here you can define any key => value pair with string or numeric value
  // that will be passed into all custom defined shell scripts.
  'custom variables' => array(
    'MY_VAR' => 'some value',
  ),

  // Unlike the build script the pre install script will be triggered on each
  // run not only during the code base build.
  //
  // Script variables:
  // - BUILD_TAG
  // - WORKSPACE
  // - BUILD_TARGET
  // - DOMAIN
  // - SCENARIO
  // - custom variables defined in the dsb.config.php
  //
  // Optional
  'pre install script' => 'preinstall.sh',

  // Same as pre install, but runs after sites get installed.
  //
  // Script variables:
  // - BUILD_TAG
  // - WORKSPACE
  // - BUILD_TARGET
  // - DOMAIN
  // - SCENARIO
  // - custom variables defined in the dsb.config.php
  //
  // Optional
  'post install script' => 'preinstall.sh',

  // Hosts configuration.
  // Note that name of hosts must be unique also between different scenarios.
  'hosts' => array(

    // Minimalistic configuration providing only admin credentials.
    // This will install the host with default installation profile.
    'example_host1' => array(

      // Site admin login.
      //
      // Optional [admin]
      'account name' => 'admin',

      // Site admin password.
      //
      // Optional [pass]
      'account pass' => 'pass',

      // Installation profile to be used to install the site.
      //
      // Optional [standard]
      'install profile' => 'standard',

      // Database name for the site.
      //
      // Optional [BUILD TAG_HOST]
      'db name' => 'my_project_test_db',
    ),

    // Extended configuration involving custom install and setup scripts.
    'example_host2' => array(

      // Install script executed after source file build by drush make.
      //
      // If you are just fine with the standard installation using installation
      // profile you do not need an install script. This is needed only in case
      // you want to install the site either by passing into drush site-install
      // additional options, or use fully customized way.
      //
      // Optional [drush si $INSTALL_PROFILE --sites-subdir="${SITE}"
      // --db-url=$DB_URL --account-pass=$ACC_PASS --account-name=$ACC_NAME -y]
      //
      // Script variables:
      // - BUILD_TAG
      // - WORKSPACE
      // - BUILD_TARGET
      // - SCENARIO
      // - DOMAIN
      // - SITE
      // - SITE_DIR
      // - URI
      // - DB_URL
      // - ACC_NAME
      // - ACC_PASS
      // - INSTALL_PROFILE
      // - custom variables defined in the dsb.config.php
      'install script' => 'drush si my_custom_profile --sites-subdir="${SITE}" --db-url=$DB_URL --account-pass=$ACC_PASS --account-name=$ACC_NAME -y',

      // Script that gets executed after site has been installed.
      //
      // Use this script to do additional setup, i.e. running custom drush
      // commands to do additional setup or to generate testing content, etc.
      //
      // Script variables: see "install script" variables.
      //
      // Optional
      'setup script' => 'drush my-custom-setup --uri="http://$URI"',

      // Define tests that should run for this specific site.
      //
      // Tests do not need to be defined for each site, only for the one for
      // which it makes sense.
      //
      // Optional
      'tests' => array(

        // Array of test classes or groups to be run.
        //
        // Required
        'test objects' => array('"My Tests Group"'),

        // Custom script that will run tests.
        //
        // Script variables:
        // - WORKSPACE
        // - URI
        // - SITE_DIR
        //
        // Required
        'test script' => 'drush test-run --uri="http://$URI" "My Tests Group"',

        // Note that "test objects" and "custom script" are mutually exclusive, and
        //"custom script" will override "test objects".
      ),
    ),
  ),
);

// Include local config file with config overrides.
if (file_exists(dirname(__FILE__) . '/local.dsb.config.php')) {
  include dirname(__FILE__) . '/local.dsb.config.php';
}
