<?php
/**
 * @file
 * Drush scenario builder - the utility to build Drupal site scenarios.
 */

/**
 * Implements hook_drush_command().
 */
function dsb_drush_command() {
  $items = array();

  $items['dsb-scenarios-build'] = array(
    'description' => dt('Runs drush scenario builder to install and configure Drupal sites as specified in the dsb.config.php.'),
    'arguments' => array(
      'scenarios' => dt('Specific scenarios to be built.'),
    ),
    'options' => array(
      'workspace' => dt('Path to the workspace directory. Defaults to the directory where the drush dsb command was called.'),
      'build-target' => dt('Path to the project directory. Defaults to workspace/www directory.'),
      'build-tag' => dt('Build tag - the namespace for current build.'),
      'makefile' => dt('Name of the Drush make file.'),
      'run-tests' => dt('If set tests will be triggered as configured in dsb.config.php.'),
      'cleanup' => dt('Flag to run cleanup upon tests completion.'),
      'force-rebuild' => dt('Flag to force rebuild of source codes.'),
      'domain' => dt('Base domain.'),
      'db-url' => dt('DB url.'),
      'skip-checks' => dt('Flag to skip the configuration health checks.'),
      'skip-setup' => dt('Flag to skip the installation and setup. Only makes sense to use this option with --run-tests, otherwise it will not do anything.'),
      'concurrency' => dt('Enables concurrent processing if possible. Currently unlimited parallelization. Requires PCNTL Functions.'),
      'override' => dt('Extra config file to override default config.'),
    ),
    'examples' => array(
      'drush dsb --workspace=/var/www/my_project' => dt('Builds all scenarios defined in the dsb.config.php file which is located in the /var/www/my_project dir.'),
      'drush dsb my_scenario other_scenario' => dt('Builds my_scenario and other_scenario defined in the dsb.config.php file.'),
      'drush dsb --run-tests' => dt('Builds all scenarios and runs tests.'),
      'drush dsb --run-tests --cleanup' => dt('Builds all scenarios, runs tests and cleanup.'),
      'drush dsb --force-rebuild' => dt('Prior to building scenarios it will delete all sources in the build_target if found.'),
    ),
    'aliases' => array('dsb'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  return $items;
}

/**
 * Prepare workspace.
 *
 * First decide where the workspace needs to be. Current or from options.
 */
function _dsb_init_workspace() {
  $workspace = drush_cwd();
  if ($to = drush_get_option('workspace', FALSE)) {
    if (!is_dir($to)) {
      drush_set_error(dt('Invalid workspace directory @workspace', array('@workspace' => $to)));
      die();
    }
    $workspace = $to;
  }

  // Remove trailing slashes if present.
  $workspace = rtrim($workspace, '/');
  return $workspace;
}

/**
 * Prepare config.
 */
function _dsb_init_command(&$config, $args) {
  // Select scenario from args if possible.
  if (!empty($args[0])) {
    $scenario = $args[0];
    if (empty($config['scenarios'][$scenario])) {
      drush_set_error(dt('Unknown scenario @scenario', array('@scenario' => $scenario)));
      die();
    }
    $config['selected scenario'] = $scenario;
  }

  // Set first scenario if none defined.
  if (empty($config['selected scenario'])) {
    $scenarios = array_keys($config['scenarios']);
    $scenario_first = $scenarios[0];
    $config['selected scenario'] = $scenario_first;
    drush_print(dt('No scenario given. Selecting scenario @scenario', array('@scenario' => $scenario_first)));
  }

  // Set config vars from cmd input.
  if ($force_rebuild = drush_get_option('force-rebuild', FALSE)) {
    $config['force rebuild'] = $force_rebuild;
  }
  if ($domain = drush_get_option('domain', FALSE)) {
    $config['domain'] = $domain;
    $config['verbose'] && drush_print(dt('Domain: @domain', array('@domain' => $domain)));
  }
  if ($db_url = drush_get_option('db-url', FALSE)) {
    $config['db url'] = $db_url;
    $config['verbose'] && drush_print(dt('DB Url: @db-url', array('@db-url' => $db_url)));
  }
  if ($build_tag = drush_get_option('build-tag', FALSE)) {
    $config['build tag'] = $build_tag;
    $config['verbose'] && drush_print(dt('Build Tag: @build-tag', array('@build-tag' => $build_tag)));
  }
  if ($makefile = drush_get_option('makefile', FALSE)) {
    $config['makefile'] = $makefile;
    $config['verbose'] && drush_print(dt('Makefile: @makefile', array('@makefile' => $makefile)));
  }
  if ($skip_checks = drush_get_option('skip-checks', FALSE)) {
    $config['skip checks'] = $skip_checks;
    $config['verbose'] && drush_print(dt('Skip checks: @skip-checks', array('@skip-checks' => $skip_checks)));
  }
  if ($skip_setup = drush_get_option('skip-setup', FALSE)) {
    $config['skip setup'] = $skip_setup;
    $config['verbose'] && drush_print(dt('Skip setup: @skip-setup', array('@skip-setup' => $skip_setup)));
  }
  if ($run_tests = drush_get_option('run-tests', FALSE)) {
    $config['run tests'] = $run_tests;
    $config['verbose'] && drush_print(dt('Run tests: @run-tests', array('@run-tests' => $run_tests)));
  }
  if ($cleanup = drush_get_option('cleanup', FALSE)) {
    $config['cleanup'] = $cleanup;
    $config['verbose'] && drush_print(dt('Verbose: @verbose', array('@verbose' => $verbose)));
  }
  if ($concurrency = drush_get_option('concurrency', FALSE)) {
    $config['concurrency'] = $concurrency;
    $config['verbose'] && drush_print(dt('Concurrency: @concurrency', array('@concurrency' => $concurrency)));
  }

  // Cmd input.
  if ($build_target = drush_get_option('build-target', FALSE)) {
    $config['build target'] = $build_target;
    // @todo make sure this is an absolute path!
  }
  // Set default.
  elseif (empty($config['build target'])) {
    $config['build target'] = $config['workspace'] . '/www';
  }
  $config['verbose'] && drush_print(dt('Build target: @build-target', array('@build-target' => $config['build target'])));
}

/**
 * Main caller function to run a scenarios build.
 *
 * The first argument passed is considered the selected scenario.
 *
 * @return boolean
 *   Success status.
 *
 * @see _dsb_scenarios_build_check()
 * @see _dsb_scenarios_build_workspace()
 * @see _dsb_scenarios_build_install_sites()
 * @see _dsb_scenarios_build_run_tests()
 * @see _dsb_scenarios_build_cleanup()
 */
function drush_dsb_scenarios_build() {
  $args = func_get_args();

  $start = time();
  drush_print('=========== ' . dt('Starting at @time', array('@time' => date('H:i:s', $start))) . ' ===========');

  // Decide where the workspace lives.
  $workspace = _dsb_init_workspace();
  drush_print(dt('Workspace is @workspace', array('@workspace' => $workspace)));

  // Load config.
  $config = _dsb_init_config($workspace);
  // Merge the drush command options.
  _dsb_init_command($config, $args);
  $scenario = $config['selected scenario'];

  $is_error = FALSE;

  // System health check.
  if (empty($config['skip checks']) && !$is_error) {
    drush_print('=========== ' . dt('Health check of environment.') . ' ===========');
    try {
      _dsb_scenarios_build_check($config);
    }
    catch (Exception $e) {
      drush_set_error('build_error', dt('Error occurred during check process: @error', array('@error' => $e->getMessage())));
      $is_error = TRUE;
      // We need to die() in case of error.
      die();
    }
  }

  // In case we have build target remove it if requested by cmd option.
  if (file_exists($config['build target']) && !empty($config['force rebuild']) && !$is_error) {
    $config['verbose'] && drush_print(dt('Forced rebuild. Removing target @target', array('@target' => $config['build target'])));
    if (!drush_delete_dir($config['build target'], TRUE)) {
      drush_set_error(dt('Unable to remove build target directory @target.', array('@target' => $config['build target'])));
      die();
    }
  }

  // Build workspace if build target does not exists.
  if (!file_exists($config['build target']) && !$is_error) {
    drush_print('=========== ' . dt('Building project at @target', array('@target' => $config['build target'])) . ' ===========');
    try {
      _dsb_scenarios_build_workspace($config);
    }
    catch (Exception $e) {
      drush_set_error('build_error', dt('Error occurred during build process: @error', array('@error' => $e->getMessage())));
      $is_error = TRUE;
    }
  }

  // pwd phase 2: Switch pwd to the existing build target.
  if (!file_exists($config['build target'])) {
    // Fatal.
    drush_set_error(dt('Target directory @target still missing.', array('@target' => $config['build target'])));
    die();
  }
  chdir($config['build target']);
  
  // Run scenarios if we are not advised otherwise.
  if (empty($config['skip setup']) && !$is_error) {
    $config_scenario = $config['scenarios'][$scenario];

    drush_print('=========== ' . dt('Running scenario @scenario', array('@scenario' => $scenario)) . ' ===========');

    // Install all sites in the scenario.
    try {
      _dsb_scenarios_build_install_sites($config, $config['build tag'], $config['workspace'], $config['build target'],
        $config['db url'], $config['domain'], $scenario, $config_scenario);
    }
    catch (Exception $e) {
      drush_set_error('build_error', dt('Error occurred during scenario @scenario installation: @error',
        array('@error' => $e->getMessage(), '@scenario' => $scenario)));
      $is_error = TRUE;
      break;
    }
  }

  // If requested run tests.
  if (!empty($config['run tests']) && !$is_error) {
    $config_scenario = $config['scenarios'][$scenario];

    if (empty($config_scenario['tests'])) {
      drush_print('=========== ' . dt('No tests for scenario @name', array('@name' => $scenario)). ' ===========');
      continue;
    }

    drush_print('=========== ' . dt('Running tests for scenario @name', array('@name' => $scenario)). ' ===========');

    // Run all tests in scenario.
    try {
      _dsb_scenarios_build_run_tests($config, $config['workspace'], $config['build target'], $config['domain'], $config_scenario);
    }
    catch (Exception $e) {
      drush_set_error('build_error', dt('Error occurred during scenario @scenario test execution: @error',
        array('@error' => $e->getMessage(), '@scenario' => $scenario)));
      $is_error = TRUE;
      break;
    }
  }

  // If requested, run cleanup for scenarios. Independent of error state.
  if (!empty($config['cleanup'])) {
    drush_print('=========== ' . dt('Running cleanup') . ' ===========');
    try {
      _dsb_scenarios_build_cleanup($config);
    }
    catch (Exception $e) {
      drush_set_error('cleanup_error', dt('Error occurred during cleanup: @error',
        array('@error' => $e->getMessage())));
      $is_error = TRUE;
    }
  }

  $end = time();
  drush_print('=========== ' . dt('Finished at @time. Time elapsed: @elapsed', array(
    '@time' => date('H:i:s', $end),
    '@elapsed' => _dsb_format_interval($end - $start),
  )) . ' ===========');

  return TRUE;
}

/**
 * Cleanup callback.
 *
 * @param array $config
 *   The root $config.
 */
function _dsb_scenarios_build_cleanup(array $config) {

  // Determine db info from db_url.
  $db_url = parse_url($config['db url']);

  // Set general shell variables.
  $general_shell_vars = array(
    'DB_USER' => $db_url['user'],
    'DB_PASS' => $db_url['pass'],
    'DB_HOST' => $db_url['host'],
    'DB_PORT' => $db_url['port'],
    'BUILD_TARGET' => $config['build target'],
    'BUILD_TAG' => $config['build tag'],
    'WORK_SPACE' => $config['workspace'],
  );

  $scenario = $config['selected scenario'];
  $config_scenario = $config['scenarios'][$scenario];

  $shell_vars = $general_shell_vars;
  $shell_vars['SCENARIO'] = $scenario;

  // Pass into shell scripts custom variables.
  if (!empty($config_scenario['custom variables'])) {
    foreach ($config_scenario['custom variables'] as $var_key => $var_value) {
      $shell_vars[$var_key] = $var_value;
    }
  }

  // Foreach host run cleanup tasks.
  foreach ($config_scenario['hosts'] as $host => $config_host) {
    $shell_vars['DB_NAME'] = $config['build tag'] . '_' . $host;

    // Build drop database script.
    $cleanup_script = array();
    if (!empty($db_url['pass'])) {
      $cleanup_script[] = "mysqladmin -u \$DB_USER -p\$DB_PASS -f drop \$DB_NAME";
    }
    else {
      $cleanup_script[] = "mysqladmin -u \$DB_USER -f drop \$DB_NAME";
    }

    $cleanup_script_cmd = _dsb_build_shell_script($shell_vars, $cleanup_script);

    // Do not use _dsb_shell_exec() as we want to continue even if db cleanup
    // did not succeeded.
    drush_shell_exec_interactive($cleanup_script_cmd);
  }

  // Run custom cleanup script.
  if (!empty($config_scenario['cleanup script'])) {
    $custom_cleanup_script_cmd = _dsb_build_shell_script($general_shell_vars, $config_scenario['cleanup script'], $config['workspace']);
    // Do not use _dsb_shell_exec() as at this point it does not bring value.
    // It is the last cmd command, we get to know the error from the cmd output
    // and the exception stack trace would just produce unnecessary error msgs.
    drush_shell_exec_interactive($custom_cleanup_script_cmd);
  }
}

/**
 * Sites installer callback.
 *
 * @param string $build_tag
 *   Build tag.
 * @param string $workspace
 *   Workspace.
 * @param string $build_target
 *   Build target directory path.
 * @param string $db_url
 *   DB url.
 * @param string $domain
 *   Base domain.
 * @param $scenario
 *   Current scenario.
 * @param array $config_scenario
 *   The current scenario config part.
 *
 * @throws Exception
 *   If there was an error executing script.
 */
function _dsb_scenarios_build_install_sites($config, $build_tag, $workspace, $build_target, $db_url, $domain, $scenario, array $config_scenario) {
  // General shell variables.
  $general_shell_vars = array(
    'BUILD_TAG' => $build_tag,
    'WORKSPACE' => $workspace,
    'BUILD_TARGET' => $build_target,
    'SCENARIO' => $scenario,
    'DOMAIN' => $domain,
  );

  // Pass into shell scripts custom variables.
  if (!empty($config_scenario['custom variables'])) {
    foreach ($config_scenario['custom variables'] as $var_key => $var_value) {
      $general_shell_vars[$var_key] = $var_value;
    }
  }
  // CD into drupal directory.
  $cd_build_target_dir_cmd = "cd {$build_target}\n";

  // Run pre install script if provided.
  if (!empty($config_scenario['pre install script'])) {
    $pre_install_script_cmd = _dsb_build_shell_script($general_shell_vars, $config_scenario['pre install script'], $workspace);
    _dsb_shell_exec($cd_build_target_dir_cmd . $pre_install_script_cmd);
  }

  // Build sites.
  $sites_php = "<?php\n\n";
  foreach ($config_scenario['hosts'] as $host => $config_host) {
    $fqdn = $host . '.' . $domain;
    // Make sites dir if not exists.
    if (!file_exists($build_target . '/sites/' . $host)) {
      drush_mkdir($build_target . '/sites/' . $host);
    }

    $sites_php .= "\$sites['${fqdn}'] = '${host}';\n";
  }

  // Create the sites.php file to enable Drupal multisite.
  $sites_filename = $build_target . '/sites/sites.php';
  if (!file_put_contents($sites_filename, $sites_php)) {
    drush_set_error(dt('Cannot write file in @file', array('@file' => $sites_filename)));
    die();
  }

  if (!empty($config['concurrency'])) {
    drush_print('=========== ' . dt('CONCURRENT Setup starts.') . ' ===========');
  }

  $pids = array();
  foreach ($config_scenario['hosts'] as $host => $config_host) {
    $pid = 0;
    if (!empty($config['concurrency'])) {
      $pid = pcntl_fork();
    }

    if ($pid == -1) {
      die('could not fork');
    }
    else if ($pid) {
      $pids[$pid] = array(
        'host' => $host,
      );
      $config['verbose'] && drush_print(dt('Child pid @pid for host @host.', array('@pid' => $pid, '@host' => $host)));
      continue;
    }
    else {
      // We are the child OR not concurrent.
      try {
        $time_start = time();
        // CD into site directory. @todo protect foldername
        $cd_site = "cd {$build_target}/sites/{$host}\n";

        $shell_vars = $general_shell_vars;

        // Add host specific variables.
        $shell_vars['SITE'] = $host;
        $shell_vars['SITE_DIR'] = $build_target . '/sites/' . $host;
        $shell_vars['ACC_NAME'] = !empty($config_host['acc_name']) ? $config_host['acc_name'] : 'admin';
        $shell_vars['ACC_PASS'] = !empty($config_host['acc_pass']) ? $config_host['acc_pass'] : 'pass';
        $shell_vars['INSTALL_PROFILE'] = !empty($config_host['install profile']) ? $config_host['install profile'] : 'standard';
        // Allow to define custom database name.
        if (!empty($config_host['db name'])) {
          $shell_vars['DB_URL'] = "{$db_url}/{$config_host['db name']}";
        }
        else {
          $shell_vars['DB_URL'] = "{$db_url}/{$build_tag}_{$host}";
        }
        // If we are installing into default site, use the base domain.
        if ($host == 'default') {
          $shell_vars['URI'] = $domain;
        }
        else {
          $shell_vars['URI'] = "{$host}.{$domain}";
        }

        // Try to load custom install script.
        $install_script = array();
        if (!empty($config_host['install script'])) {
          $install_script = $config_host['install script'];
        }
        // If none provided get the default one.
        else {
          $install_script[] = "drush si \$INSTALL_PROFILE --sites-subdir=\"\${SITE}\" --db-url=\$DB_URL --account-pass=\$ACC_PASS --account-name=\$ACC_NAME -y\ndrush status --uri=\"http://\${URI}\"";
        }

        $install_script_cmd = _dsb_build_shell_script($shell_vars, $install_script, $workspace);

        // Execute the install part.
        $interactive = empty($config['concurrency'])?TRUE:FALSE;
        _dsb_shell_exec($cd_site . $install_script_cmd, $interactive);
        //$output = drush_shell_exec_output();

        // Run setup script if any.
        if (!empty($config_host['setup script'])) {
          $setup_script_cmd = _dsb_build_shell_script($shell_vars, $config_host['setup script'], $workspace);
          _dsb_shell_exec($cd_site . $setup_script_cmd, $interactive);
          //$output = array_merge($output, drush_shell_exec_output());
        }

        $time_end = time();
        $config['verbose'] && drush_print(dt('Child host @host finished at @time. Time elapsed: @elapsed', array(
          '@host' => $host,
          '@time' => date('H:i:s', $time_end),
          '@elapsed' => _dsb_format_interval($time_end - $time_start),
        )));
      }
      catch (Exception $e) {
        if (!empty($config['concurrency'])) {
          // Convert to an exit error so the parent process knows about failure.
          drush_set_error('child_error', dt('Children aborted with error: @error', array('@error' => $e->getMessage())));
          exit(11);
        }
        else {
          throw $e;
        }
      }
      if (!empty($config['concurrency'])) {
        // The child needs to exit as the parent continues below waiting for right that.
        // Tell drush this is a clean process completion.
        drush_bootstrap_finish();
        exit(0);
      }
    }
  }

  if (!empty($config['concurrency'])) {
    // Wait for setup children to complete.
    $config['verbose'] && drush_print(dt('Wait for worker children to complete.'));
    $is_error = FALSE;
    $error_hosts = array();
    foreach ($pids as $pid => $p) {
      pcntl_waitpid($pid, $status);
      $config['verbose'] && drush_print(dt('Child pid @pid for host @host completed with status @status.', array('@pid' => $pid, '@host' => $p['host'], '@status' => $status)));
      if ($code = pcntl_wexitstatus($status)) {
        drush_print(dt('Error in pid @pid for host @host with code @code.', array('@pid' => $pid, '@host' => $p['host'], '@code' => $code)));
        $is_error = TRUE;
        $error_hosts[] = $p['host'];
      }
    }
    drush_print('=========== ' . dt('CONCURRENT Setup ends.') . ' ===========');
    if ($is_error) {
      throw new Exception(dt('Error in child execution for hosts @hosts.', array('@hosts' => implode(',', $error_hosts))));
    }
  }

  // Run post install script if provided.
  if (!empty($config_scenario['post install script'])) {
    $post_install_script_cmd = _dsb_build_shell_script($general_shell_vars, $config_scenario['post install script'], $workspace);
    _dsb_shell_exec($cd_build_target_dir_cmd . $post_install_script_cmd);
  }
}

/**
 * Test runner callback.
 *
 * @param string $workspace
 *   Workspace dir path.
 * @param string $build_target
 *   Path to the build target directory.
 * @param string $domain
 *   Base domain.
 * @param array $config_hosts
 *   The current scenario hosts config part.
 *
 * @throws Exception
 *   If there was an error executing script.
 */
function _dsb_scenarios_build_run_tests($config, $workspace, $build_target, $domain, array $config_scenario) {

  if (!empty($config['concurrency'])) {
    drush_print('=========== ' . dt('CONCURRENT Testing starts.') . ' ===========');
  }

  $pids = array();
  // First run all regular parallelizable drupal tests.
  foreach ($config_scenario['tests'] as $key => $config_test) {
    $pid = 0;
    if (!empty($config['concurrency'])) {
      $pid = pcntl_fork();
    }

    if ($pid == -1) {
      die('could not fork');
    }
    else if ($pid) {
      $pids[$pid] = array(
        'key' => $key,
      );
      $config['verbose'] && drush_print(dt('Child pid @pid for host @key.', array('@pid' => $pid, '@key' => $key)));
      continue;
    }
    else {
      // We are the child OR not concurrent.
      try {
        $time_start = time();

        // @todo init needs only one execution per host.
        $tests_script = '';
        // Start with the first host ad default.
        $hosts = array_keys($config_scenario['hosts']);
        $host = $hosts[0];
        if (!empty($config_test['host'])) {
          $host = $config_test['host'];
        }
        $host_dir = $build_target . '/sites/' . $host;
        $init_script_cmd = "cd {$host_dir}\n";
        $init_script_cmd .= "drush en simpletest -y\n";

        $shell_vars = array(
          'WORKSPACE' => $workspace,
          'URI' => $host . '.' . $domain,
          'SITE_DIR' => $host_dir,
        );

        if (!empty($config_test['command'])) {
          $tests_script = $config_test['command'];
        }
        elseif (!empty($config_test['test objects'])) {
          foreach ($config_test['test objects'] as $test) {
            // @todo these can be parallelized even further.
            // @todo this doesn't support --xml
            $tests_script .= "drush test-run --dirty --uri=\"\$URI\" $test\n";
          }
        }

        // If no test script was build, continue.
        if (!empty($tests_script)) {
          $interactive = empty($config['concurrency'])?TRUE:FALSE;
          $tests_script_cmd = _dsb_build_shell_script($shell_vars, $tests_script);

          try {
            _dsb_shell_exec($init_script_cmd . $tests_script_cmd, $interactive);
          }
          catch (Exception $e) {
            drush_print(dt('Text execution error: @error', array('@error' => $e->getMessage())));
            // We want to continue execution on error.
          }
        }

        $time_end = time();
        $config['verbose'] && drush_print(dt('Child host @host finished at @time. Time elapsed: @elapsed', array(
          '@host' => $host,
          '@time' => date('H:i:s', $time_end),
          '@elapsed' => _dsb_format_interval($time_end - $time_start),
        )));
      }
      catch (Exception $e) {
        if (!empty($config['concurrency'])) {
          drush_set_error('child_error', dt('Children aborted with error: @error', array('@error' => $e->getMessage())));
          exit(12);
        }
        else {
          throw $e;
        }
      }
      if (!empty($config['concurrency'])) {
        // The child needs to exit as the parent continues below waiting for right that.
        // Tell drush this is a clean process completion.
        drush_bootstrap_finish();
        exit(0);
      }
    }
  }

  if (!empty($config['concurrency'])) {
    // Wait for tests children to complete.
    $config['verbose'] && drush_print(dt('Wait for worker children to complete.'));
    $is_error = FALSE;
    $error_hosts = array();
    foreach ($pids as $pid => $p) {
      pcntl_waitpid($pid, $status);
      $config['verbose'] && drush_print(dt('Child pid @pid for host @host completed with status @status.', array('@pid' => $pid, '@host' => $p['host'], '@status' => $status)));
      if ($code = pcntl_wexitstatus($status)) {
        drush_print(dt('Error in pid @pid for host @host with code @code.', array('@pid' => $pid, '@host' => $p['host'], '@code' => $code)));
        $is_error = TRUE;
        $error_hosts[] = $p['host'];
      }
    }
    drush_print('=========== ' . dt('CONCURRENT Testing ends.') . ' ===========');
    if ($is_error) {
      throw new Exception(dt('Error in child execution for hosts @hosts.', array('@hosts' => implode(',', $error_hosts))));
    }
  }

  // Second run all sequential dsb drupal tests.
  if (!empty($config_scenario['sequential tests'])) {
    drush_print('=========== ' . dt('SEQUENTIAL Testing starts.') . ' ===========');
    foreach ($config_scenario['sequential tests'] as $key => $config_test) {
        $time_start = time();

        // @todo init needs only one execution per host.
        $tests_script = array();
        // Start with the first host ad default.
        $hosts = array_keys($config_scenario['hosts']);
        $host = $hosts[0];
        if (!empty($config_test['host'])) {
          $host = $config_test['host'];
        }
        $host_dir = $build_target . '/sites/' . $host;
        // Enable Simpletest.
        $init_script_cmd = "cd {$host_dir}\n";
        $init_script_cmd .= "drush en simpletest -y\n";

        $shell_vars = array(
          'WORKSPACE' => $workspace,
          'URI' => $host . '.' . $domain,
          'SITE_DIR' => $host_dir,
        );

        if (!empty($config_test['command'])) {
          $tests_script[] = $config_test['command'];
        }
        elseif (!empty($config_test['test objects'])) {
          foreach ($config_test['test objects'] as $test) {
            // @todo these can be parallelized even further.
            // @todo this doesn't support --xml
            $tests_script[] = "drush test-run --dirty --uri=\"\$URI\" $test\n";
          }
        }

        // If no test script was build, continue.
        if (!empty($tests_script)) {
          $interactive = empty($config['concurrency'])?TRUE:FALSE;
          $tests_script_cmd = _dsb_build_shell_script($shell_vars, $tests_script);

          try {
            _dsb_shell_exec($init_script_cmd . $tests_script_cmd, $interactive);
          }
          catch (Exception $e) {
            drush_print(dt('Text execution error: @error', array('@error' => $e->getMessage())));
            // We want to continue execution on error.
          }
        }

        $config['verbose'] && drush_print(dt('Sequential test @key completed.', array(
          '@key' => $key,
        )));
    }
    drush_print('=========== ' . dt('SEQUENTIAL Testing ends.') . ' ===========');
  }
}

/**
 * Health check.
 *
 * This function will die() on error.
 *
 * @param array $config
 *   The complete $config array as defined in the dsb.config.php.
 *
 * @throws Exception
 *   If there was an error executing script.
 */
function _dsb_scenarios_build_check($config) {
  $is_error = FALSE;

  $scenario = $config['selected scenario'];
  $config_scenario = $config['scenarios'][$scenario];

  // Check DNS resolution.
  foreach ($config_scenario['hosts'] as $host => $config_host) {
    $domain = $config['domain'];
    $fqdn = $host . '.' . $domain;
    // Check if host is reachable through DNS.
    if (gethostbyname($fqdn)==$fqdn) {
      drush_print(dt('Cannot resolve DNS host @host as @fqdn.', array('@host' => $host, '@fqdn' => $fqdn)));
      $is_error = TRUE;
      continue;
    }
    $config['verbose'] && drush_print(dt('Host @host as @fqdn resolves through DNS.', array('@host' => $host, '@fqdn' => $fqdn)));
  }
  if ($is_error) {
    throw new Exception('Error resolving hosts.');
  }

  // Check build target availability.
  // Try to create target folder if missing. We need it for further checks.
  $build_target = $config['build target'];
  if (!drush_mkdir($build_target)) {
    throw new Exception(dt('Inaccessible target @target.', array('@target' => $build_target)));
  }

  // Check host apache configuration.
  $checkfile = 'dsb.checkfile.html';
  $fh = fopen($build_target . '/' . $checkfile, 'w');
  if (!$fh) {
    throw new Exception(dt('Unable to write file to target @path.', array('@path' => $build_target . '/' . $checkfile)));
  }
  $content = md5(uniqid());
  fwrite($fh, $content);
  fclose($fh);
  foreach ($config_scenario['hosts'] as $host => $config_host) {
    $domain = $config['domain'];
    $fqdn = $host . '.' . $domain;
    // Check if host is reachable through HTTP.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $fqdn . '/' . $checkfile);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $output = curl_exec($ch);
    curl_close($ch);
    if ($output != $content) {
      drush_print(dt('Cannot reach host @host as @fqdn through HTTP.', array('@host' => $host, '@fqdn' => $fqdn)));
      $is_error = TRUE;
      continue;
    }
    $config['verbose'] && drush_print(dt('Host @host at @fqdn reachable through HTTP.', array('@host' => $host, '@fqdn' => $fqdn)));
  }
  unlink($build_target . '/' . $checkfile);
  if ($is_error) {
    throw new Exception(dt('Error accessing hosts through http.'));
  }

  // Check database connection.
  if (!$is_error && empty($config['db url'])) {
    drush_set_error(dt('Required config "db url" not defined.'));
    $is_error = TRUE;
  }
  $url = parse_url($config['db url']);
  // @todo support other database types.
  if ($url['scheme'] == 'mysql') {
    // The DSN should use either a socket or a host/port.
    if (isset($config['unix_socket'])) {
      $dsn = 'mysql:unix_socket=' . $config['unix_socket'];
    }
    else {
      // Default to TCP connection on port 3306.
      $dsn = 'mysql:host=' . $url['host'] . ';port=' . (empty($url['port']) ? 3306 : $url['port']);
    }
    // @todo No database selection for now.

    $db_user = NULL;
    if (!empty($url['user'])) {
      $db_user = $url['user'];
    }
    $db_pass = NULL;
    if (!empty($url['pass'])) {
      $db_pass = $url['pass'];
    }
    try {
      $driver_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
      // @todo port doesn't seem to be functional.
      $db = new PDO($dsn, $db_user, $db_pass, $driver_options);
    }
    catch(Exception $e) {
      // Don't show credentials here.
      drush_set_error(dt('Unable to connect to database.'));
      $is_error = TRUE;
      throw $e;
    }
    // @todo check permission to create databases. The scripts will fail hard without this permission.
  }

  // @todo check other required config values.

  // @todo call check script per scenario. (Possibly also per host?)

  // Check if concurrency support works.
  if ($concurrency = drush_get_option('concurrency', FALSE)) {
    if (!function_exists('pcntl_fork')) {
      drush_set_error(dt('PCNTL Functions not available. No concurrency support.'));
      die();
    }
  }
  return !$is_error;
}

/**
 * Builds workspace.
 *
 * - Invokes make which will copy all the project sources into build_target.
 * - Builds sites.php configuration and copies it to sites directory.
 *
 * @param array $config
 *   The complete $config array as defined in the dsb.config.php.
 */
function _dsb_scenarios_build_workspace($config) {
  $makefile = !empty($config['makefile']) ? $config['makefile'] : 'project.make';

  $make_script = array();
  if (!empty($config['make script'])) {
    $make_script = $config['make script'];
  }
  else {
    $make_cmd = "drush make \$MAKE_FILE \$BUILD_TARGET";
    if ($config['verbose']) {
      $make_cmd .= ' -v';
    }
    $make_script[] = $make_cmd;
  }

  $shell_vars = array(
    'BUILD_TAG' => $config['build tag'],
    'WORKSPACE' => $config['workspace'],
    'BUILD_TARGET' => $config['build target'],
  );

  // Run drush make only if we have the make file.
  if (file_exists($config['workspace'] . '/' . $makefile)) {
    $shell_vars['MAKE_FILE'] = $config['workspace'] . '/' . $makefile;
    $make_script_cmd = _dsb_build_shell_script($shell_vars, $make_script);
    _dsb_shell_exec($make_script_cmd);
  }

  // Iterate through scenarios and run custom build scripts if provided.
  $scenario = $config['selected scenario'];
  $config_scenario = $config['scenarios'][$scenario];

  // Pass into the script custom variables.
  if (!empty($config_scenario['custom variables'])) {
    foreach ($config_scenario['custom variables'] as $var_key => $var_value) {
      $shell_vars[$var_key] = $var_value;
    }
  }

  // Run custom build script if provided.
  if (!empty($config_scenario['build script'])) {

    $shell_vars['SCENARIO'] = $scenario;
    $build_script_cmd = _dsb_build_shell_script($shell_vars, $config_scenario['build script'], $config['workspace']);

    _dsb_shell_exec($build_script_cmd);
  }
}

/**
 * Loads config array from dsb.config.php file.
 *
 * @param string $workspace
 *   Path to the workspace directory.
 *
 * @return array
 *   The config array.
 */
function _dsb_init_config($workspace) {
  static $config_cache;

  if (empty($config_cache)) {
    $config = array();
    $config['workspace'] = $workspace;

    // pwd phase 1: Switch pwd to the workspace.
    chdir($config['workspace']);

    // Load main config file.
    $config_filename = $workspace . '/dsb.config.php';
    if (!file_exists($config_filename)) {
      drush_set_error(dt('Cannot read config dsb.config.php in @workspace', array('@workspace' => $workspace)));
      die();
    }
    require_once $config_filename;

    // Init some basic options.
    if ($verbose = drush_get_option('verbose', FALSE)) {
      $config['verbose'] = $verbose;
    }

    // Check for config override definition.
    if ($override = drush_get_option('override', FALSE)) {
      $config['override'] = $override;
      $config['verbose'] && drush_print(dt('Override: @override', array('@override' => $override)));
      if (!file_exists($config['override'])) {
        drush_set_error(dt('Cannot read override @override', array('@override' => $override)));
        die();
      }
      require_once $config['override'];
    }

    $config_cache = $config;
  }

  return $config_cache;
}

/**
 * Build shell script.
 *
 * @param array $vars
 *   The environment variables to set.
 * @param array $scripts
 *   The scripts to execute
 * @param string $workspace
 *   The path of the workspace.
 *
 * @return string
 *   The script to execute.
 */
function _dsb_build_shell_script(array $vars, array $scripts, $workspace = NULL) {
  $shell_script = '';
  // Prepend the variables.
  foreach ($vars as $key => $val) {
    $shell_script .= "export $key=\"\"\n";
    $shell_script .= "$key=$val\n";
  }

  // Merge the scripts to a single command.
  foreach ($scripts as $script) {
    // @todo why these two cases  here? Document them cleanly.
    if (!empty($workspace) && file_exists($workspace . '/' . $script)) {
      $shell_script .= "${workspace}/${script}\n";
    }
    else {
      $shell_script .= "${script}\n";
    }
  }

  foreach (array_keys($vars) as $key) {
    $shell_script .= "unset $key\n";
  }

  return $shell_script;
}

/**
 * Variant of format_interval().
 *
 * @see format_interval()
 */
function _dsb_format_interval($interval, $granularity = 2, $langcode = NULL) {
  $units = array(
    '1 year|@count years' => 31536000,
    '1 month|@count months' => 2592000,
    '1 week|@count weeks' => 604800,
    '1 day|@count days' => 86400,
    '1 hour|@count hours' => 3600,
    '1 min|@count min' => 60,
    '1 sec|@count sec' => 1
  );
  $output = '';
  foreach ($units as $key => $value) {
    $key = explode('|', $key);
    if ($interval >= $value) {
      $output .= ($output ? ' ' : '') . _dsb_format_plural(floor($interval / $value), $key[0], $key[1], array(), array('langcode' => $langcode));
      $interval %= $value;
      $granularity--;
    }

    if ($granularity == 0) {
      break;
    }
  }
  return $output ? $output : dt('0 sec', array(), array('langcode' => $langcode));
}

/**
 * Variant of format_plural().
 *
 * @see format_plural().
 */
function _dsb_format_plural($count, $singular, $plural, array $args = array(), array $options = array()) {
  $args['@count'] = $count;
  if ($count == 1) {
    return dt($singular, $args, $options);
  }

  $index = -1;
  // If the index cannot be computed, use the plural as a fallback (which
  // allows for most flexiblity with the replaceable @count value).
  if ($index < 0) {
    return dt($plural, $args, $options);
  }
  else {
    switch ($index) {
      case "0":
        return dt($singular, $args, $options);
      case "1":
        return dt($plural, $args, $options);
      default:
        unset($args['@count']);
        $args['@count[' . $index . ']'] = $count;
        return dt(strtr($plural, array('@count' => '@count[' . $index . ']')), $args, $options);
    }
  }
}

/**
 * Wrapper to execute shell script.
 *
 * @todo - does not work as drush_shell_exec_interactive() will not return FALSE
 * on shell command not found.
 *
 * @param string $script
 *   Script to execute.
 *
 * @throws Exception
 *   If there was an error executing script.
 */
function _dsb_shell_exec($script, $interactive = TRUE) {
  if (!$interactive) {
    if (!drush_shell_exec($script)) {
      throw new Exception(dt('Error executing script @script', array('@script' => substr($script, 0, 250))));
    }
    return;
  }
  if (!drush_shell_exec_interactive($script)) {
    throw new Exception(dt('Error executing script @script', array('@script' => substr($script, 0, 250))));
  }
}
